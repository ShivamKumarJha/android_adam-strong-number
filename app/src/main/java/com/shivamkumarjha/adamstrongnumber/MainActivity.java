package com.shivamkumarjha.adamstrongnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    //UI references
    public Button b1, b2;
    public EditText e1;
    public TextView t1,t2;

    public void init() {
        //Match ID's
        b1 = (Button) findViewById(R.id.button1);
        b2 = (Button) findViewById(R.id.button2);
        e1 = (EditText) findViewById(R.id.number);
        t1 = (TextView) findViewById(R.id.isadam);
        t2= (TextView)findViewById(R.id.isstrong);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        onClick();
    }

    public void onClick()
    {
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(1);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isedittextempty(2);
            }
        });
    }

    public void isedittextempty(Integer n)
    {
        init();
        String number = e1.getText().toString();
        if (TextUtils.isEmpty(number)) {
            e1.setError(getString(R.string.hint));
        }
        else if (n==1)
        {
            isadam();
        }
        else if (n==2)
        {
            isstrong();
        }
    }

    public void isadam() {
        String number = e1.getText().toString();

        int r,p,m,n,rev,a,c;
        r=0;
        rev=0;
        n= Integer.parseInt(number);
        c=n*n;
        while(n!=0)
        {
            m=n%10;
            r=r*10+m;
            n=n/10;
        }
        p=r*r;
        while(c!=0)
        {
            a=c%10;
            rev=rev*10+a;
            c=c/10;
        }
        if(rev==p)
            t1.setText("YES");
        else
            t1.setText("NO");
    }

    public void isstrong(){
        String number = e1.getText().toString();

        int input, temp, mod, sum = 0;
        input= Integer.parseInt(number);
        temp = input;
        while(input > 0)
        {
            mod = input % 10;
            sum=sum + fact (mod);
            input  = input /10;
        }
        if(temp == sum)
            t2.setText("YES");
        else
            t2.setText("NO");
    }

    public int fact(int num)
    {
        int fact=1;

        for(int i = 1; i <= num; i++)
        {
            fact *= i;
        }
        return fact;
    }
}
